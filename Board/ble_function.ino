String parseString(String data, char separator, int index){
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;
  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  } 
  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();
      dataMasuk = "";
      if (rxValue.length() > 0) {
        prString("*********");
        prString("Received Value: ");
        for (int i = 0; i < rxValue.length(); i++){
          dataMasuk += rxValue[i];
          prStringT(String(rxValue[i]));
        }
        prString("");
        prString(dataMasuk);
        prString("*********");

        String ID_Dev = parseString(dataMasuk,';',0);
        cmd = parseString(dataMasuk,';',1);

        prString("ID DEV : ");
        prString(ID_Dev);
        prString("CMD : ");
        prString(cmd);

        if (ID_Dev == iddev){
          if (cmd == "TEST"){
            prString("TEST");
            dataKeluar = "test";
          }else if (cmd == "UNLK"){
            vbatt();
            if (batt < 25){
              prString("Batre Low");
              prInt(batt);
              dataKeluar = "low";
            }else{
              prString("BERHASIL");
              stat = "UNLOCK";
              dataKeluar = "true";
              digitalWrite(HLock, HIGH);
              delay(1000);
              digitalWrite(HLock, LOW);
              powerSIM808(true);        // on SIM808 
            }
          }else if (cmd == "DEBUGON"){
            prString("DEBUG ON");
            dataKeluar = "debugon";
            debug = true;
          }else if (cmd == "DEBUGOFF"){
            prString("DEBUG OFF");
            dataKeluar = "debugoff";
            debug = false;
          }else{
            dataKeluar = "error";
          }
        }else{
          dataKeluar = "false";
        }

        if (deviceConnected) {           
          char txString[200]; // make sure this is big enuffz
          dataKeluar.toCharArray(txString, 200);
  
          pTxCharacteristic->setValue(txString);
          pTxCharacteristic->notify();  // Send the value to the app!
          delay(10); // bluetooth stack will go into congestion, if too many packets are sent
        }
      }
    }
};

void BLEsetup(){
  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
                    CHARACTERISTIC_UUID_TX,
                    BLECharacteristic::PROPERTY_NOTIFY
                  );
                      
  pTxCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
                       CHARACTERISTIC_UUID_RX,
                      BLECharacteristic::PROPERTY_WRITE
                    );

  pRxCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  //Serial.println("Waiting a client connection to notify...");
}
