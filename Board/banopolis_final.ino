#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <TinyGPS++.h>

BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;
byte txValue = 0;
byte flag = 0;

String dataMasuk;
String dataKeluar;
String cmd;

#define PRIVAT_ID "xxx"

#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"


TinyGPSPlus gps;

#define RXD2 16
#define TXD2 17
#define SIM Serial2
#define HLock 23
#define senseHLock 22
#define powerSIM 21

//Read ADC 4.2V - R1 20K R2 10K
#define adc 34

//Read ADC 42V - R1 120K R2 10K
#define adc2 33

#define DIVIDER 0.18
#define VREF  3.3

const String iddev = "1";                   // ---------------  ID DEVICE ------------------
const String key = "Bn2020Xyz";             // ---------------  KEY WEB SERVER ------------------
const char* BLEname = "Banopolis";          // ---------------  BLE NAME ------------------

bool debug = false;
bool statPWR = false;

String Server = "http://nama servernya";  // --------------- SERVER ------------------
String URL = "AT+HTTPPARA=\"URL\",\""+Server+"/api/updatedev?key="+key+"&iddev="+iddev;

String dataSIM;
String stat = "LOCK";
String Lat;
String Lon;
String Speed;
String dTime;
int batt;

unsigned long timeUpdate;
unsigned long intervalUpdate;
unsigned long unlockUpdate = 60000;   //  60 detik
unsigned long lockUpdate = 1800000;   //  30 menit (dalam mili detik)


void setup() {
  Serial.begin(115200);
  SIM.begin(9600, SERIAL_8N1, RXD2, TXD2);
  
  // Create the BLE Device
  BLEDevice::init(BLEname);

  pinMode(powerSIM, OUTPUT);
  digitalWrite(powerSIM, LOW);
  
  pinMode(HLock, OUTPUT);
  digitalWrite(HLock, LOW);
  
  pinMode(senseHLock, INPUT_PULLUP);

  BLEsetup();

  if (digitalRead(senseHLock) == 1){        // 1 Lock   |---| 0 Unlock
    prString("Sepeda dalam keadaan Lock");
    intervalUpdate = lockUpdate;
    stat = "LOCK";
  }else{
    prString("Sepeda dalam keadaan UnLock");
    intervalUpdate = unlockUpdate;
    stat = "UNLOCK";
  }

  powerSIM808(true);               // sim808 - ON
  delay(5000);
  
  vbatt();
  getGPS();
  request();
  
  timeUpdate = millis();
}

void powerSIM808(bool flag){
  if (flag){
    statPWR = true;
    prString("Turn ON SIM808");
    digitalWrite(powerSIM, HIGH);       // sim808 - ON

    prString("Waiting for network");
    delay(2500);

    prString("Enable GPS");
    SIM.println("AT+CGPSPWR=1");
    ShowResponse(3000);
    delay(100);
  }else{
    statPWR = false;
    prString("---- Turn OFF SIM808 -----");
    digitalWrite(powerSIM, LOW);
  }
}

void loop() {

    // disconnecting
    if (!deviceConnected && oldDeviceConnected) {
        delay(500); // give the bluetooth stack the chance to get things ready
        pServer->startAdvertising(); // restart advertising
        //Serial.println("start advertising");
        oldDeviceConnected = deviceConnected;
    }
    // connecting
    if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
        oldDeviceConnected = deviceConnected;
    }

    if (digitalRead(senseHLock) == 1){        // 1 Lock   |---| 0 Unlock
      if (flag == 0){
        flag++;
        prString("Melakukan Lock Sepeda");
        dataKeluar = "lock";
        if (deviceConnected) {           
          char txString[200]; // make sure this is big enuffz
          dataKeluar.toCharArray(txString, 200);
  
          pTxCharacteristic->setValue(txString);
          pTxCharacteristic->notify();  // Send the value to the app!
          delay(10); // bluetooth stack will go into congestion, if too many packets are sent
        }
        intervalUpdate = lockUpdate;
        stat = "LOCK";
        vbatt();
        getGPS();
        request();
        powerSIM808(false);        // off SIM808 
      }else{
        flag = 2;
        if (statPWR){
          powerSIM808(false);        // off SIM808 
        }
      }
    }else{
      flag = 0;
      intervalUpdate = unlockUpdate;
    }
//    Serial.println(millis());
//    Serial.println(timeUpdate);
    if(millis() - timeUpdate >= intervalUpdate){
      if (!statPWR){
        powerSIM808(true);        // on SIM808 
        delay(10000);
      }
      vbatt();
      getGPS();
      request();
      timeUpdate = millis();
    }
}

void vbatt(){
  int dataADC=0;
  for (byte z=0; z<10; z++){
    dataADC += analogRead(adc);
  }
  dataADC /= 10; 
  float volt = ((dataADC/4095.0) * VREF) + DIVIDER;
  float voltIN = volt * 3.03;     //R1-20K | R2-10K
  batt = ((voltIN-3.0)/(4.2-3.0)) * 100;    // 3.0 == 0% | 4.2 == 100%
  if(batt > 100){
    batt = 100;
  }
  if(batt < 0){
    batt = 0;
  }
  prStringT("adc : ");
  prInt(dataADC);
  prStringT("volt divide : ");
  prFloat(volt);
  prStringT("volt battery : ");
  prFloat(voltIN);
  prStringT("battery % : ");
  prInt(batt);
}

void vbatt2(){
  int dataADC2=0;
  for (byte z=0; z<10; z++){
    dataADC2 += analogRead(adc2);
  }
  dataADC2 /= 10; 
  float volt = ((dataADC2/4095.0) * VREF) + DIVIDER;
  float voltIN = volt * 3.03;     //R1-120K | R2-10K
  batt = ((voltIN-30.0)/(42.0-30.0)) * 100;    // 30.0 == 0% | 42.0 == 100%
  if(batt > 100){
    batt = 100;
  }
  if(batt < 0){
    batt = 0;
  }
  prStringT("adc2 : ");
  prInt(dataADC2);
  prStringT("volt2 divide : ");
  prFloat(volt);
  prStringT("volt battery 2 : ");
  prFloat(voltIN);
  prStringT("battery2 % : ");
  prInt(batt);
}

void getGPS(){
  prString("Power GPS");
  SIM.println("AT+CGPSPWR=1");
  ShowResponse(3000);

  prString("Status GPS");
  SIM.println("AT+CGPSSTATUS?");
  GetResponse(3000);

  prString("Info GPS");
  SIM.println("AT+CGPSINF=0");
  GetResponse(3000);

  prString("Request Location GPS");
  SIM.println("AT+CGPSOUT=32");
  ParseLocationGPS(3000);

  prString("Disable Request Location GPS");
  SIM.println("AT+CGPSOUT=0");
  ShowResponse(3000);

//  Serial.println("OFF GPS POwer");
//  SIM.println("AT+CGPSPWR=0");
//  ShowResponse(3000);
}

void request(){
  prString("AT+SAPBR-GPRS");
  SIM.println("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"");
  ShowResponse(3000);

  prString("AT+SAPBR-APN");
  SIM.println("AT+SAPBR=3,1,\"APN\",\"internet\"");
  ShowResponse(3000);

  prString("AT+SAPBR-1,1");
  SIM.println("AT+SAPBR=1,1");
  ShowResponse(5000);

  prString("AT+INIT");
  SIM.println("AT+HTTPINIT");
  ShowResponse(5000);

  prString("--------URL----------");
  prString(URL);
//  SIM.println(URL);
  SIM.print(URL);
  SIM.print("&status=");
  SIM.print(stat);
  SIM.print("&batt=");
  SIM.print(batt);
  SIM.print("&lat=");
  SIM.print(Lat);
  SIM.print("&lon=");
  SIM.print(Lon);
  SIM.print("&speed=");
  SIM.print(Speed);
  SIM.print("&dtime=");
  SIM.print(dTime);
  SIM.println("\"");
  ShowResponse(3000);
  
  // set http action type 0 = GET, 1 = POST, 2 = HEAD
  prString("AT+ACTION");
  SIM.println("AT+HTTPACTION=0");
  GetResponse(6000);

  prString("AT+READ");
  SIM.println("AT+HTTPREAD");
  GetResponse(2000);

  prString("AT+TERMINATE");
  SIM.println("AT+HTTPTERM");
  GetResponse(3000);

  prString("AT+SAPBR-0,1");
  SIM.println("AT+SAPBR=0,1");
  GetResponse(3000);
}

void ShowResponse(int wait){
  dataSIM = "";
  prString("response : ");
  long timeNOW = millis();
  while(millis()-timeNOW < wait){
    if(SIM.find("OK")){
      prString("GOOD");
      return;
    }
  }
  prString("error time out");
}

void GetResponse(int wait){
  dataSIM = "";
  prString("response : ");
  long timeNOW = millis();
  while(millis()-timeNOW < wait){
    while(SIM.available()>0){
      dataSIM += (char)SIM.read();
    }
  }
  prString(dataSIM);
  prString("");
}

void ParseLocationGPS(int wait){
  dataSIM = "";
  prString("response : ");
  long timeNOW = millis();
  while(millis()-timeNOW < wait){
    if (SIM.available()) {
      if (gps.encode(SIM.read())){
        displayInfo();
      }
    }
  }
  prString(dataSIM);
  prString("");
}

void displayInfo()
{
  prString("Location: "); 
  if (gps.location.isValid())
  {
    Lat = String(gps.location.lat(), 6);
    Lon = String(gps.location.lng(), 6);
    prStringT(Lat);
    prStringT(",");
    prStringT(Lon);
  }
  else
  {
    Lat = "0";
    Lon = "0";
    prStringT("INVALID");
  }

  prString("  Date/Time: ");
  if (gps.date.isValid())
  {
    String Month;
    String Day;
    String Year;

    if(gps.date.day() < 10){
      Day = "0";
      prStringT("0");
    }
    
    Day += String(gps.date.day());
    prStringT(Day);
    prStringT("/");
    if(gps.date.month() < 10){
      Month = "0";
      prStringT("0");
    }
    
    Month += String(gps.date.month());
    prStringT(Month);
    prStringT("/");
    Year = String(gps.date.year());
    prStringT(Year);
    dTime = Year + "-" + Month + "-" + Day + "T";
  }
  else
  {
    dTime = "INVALID,T";
    prStringT("INVALID");
  }

  prStringT(" ");
  if (gps.time.isValid())
  {
    String Hour;
    String Minute;
    String Second;
    if (gps.time.hour() < 10) {
      Hour = "0";
      prStringT("0");
    }
    Hour += String(gps.time.hour());
    prStringT(Hour);
    prStringT(":");
    if (gps.time.minute() < 10) {
      Minute = "0";
      prStringT("0");
    }
    Minute += String(gps.time.minute());
    prStringT(Minute);
    prStringT(":");
    if (gps.time.second() < 10) {
      Second = "0";
      prStringT("0");
    }
    Second += String(gps.time.second());
    prStringT(Second);
    prStringT(".");
    if (gps.time.centisecond() < 10) prStringT("0");
    prIntT(gps.time.centisecond());
    dTime += Hour + ":" + Minute + ":" + Second;
  }
  else
  {
    dTime += "INVALID";
    prStringT("INVALID");
  }

  prStringT(" Speed: ");
  if(gps.speed.isValid()){
    Speed = String(gps.speed.kmph());
    prStringT(Speed);
  }else{
    Speed = "0";
    prStringT("INVALID");
  }

  prString("");
}

void prString(String strng){
  if(debug){
    Serial.println(strng);
  }
}

void prStringT(String strng){
  if(debug){
    Serial.print(strng);
  }
}

void prInt(long dtlong){
  if(debug){
    Serial.println(dtlong);
  }
}

void prIntT(long dtlong){
  if(debug){
    Serial.print(dtlong);
  }
}

void prFloat(float dtfloat){
  if(debug){
    Serial.println(dtfloat);
  }
}
