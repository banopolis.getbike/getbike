package com.example.banogetbike;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class BanoBike_About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bano_bike__about);

        Button btnLearnMore = findViewById(R.id.btn_learn_more);

        btnLearnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://banopolis.id/";
                Intent intent = new Intent(BanoBike_About.this, BanoBike_WebActivity.class);
                intent.putExtra(BanoBike_WebActivity.EXTRA_URL, url);
                startActivity(intent);
            }
        });
    }
}
