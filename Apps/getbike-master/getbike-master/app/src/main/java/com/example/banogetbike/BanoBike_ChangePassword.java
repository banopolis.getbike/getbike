package com.example.banogetbike;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class BanoBike_ChangePassword extends AppCompatActivity {
    private EditText current_pass, new_pass, confirm_pass;
    private Button change_pass;

    private FirebaseAuth mAuth;

    private String userEmail;

    private final String TAG = "ChangePassword";

    public final static String EXTRA_EMAIL = "extra_email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bano_bike__change_password);

        Log.d(TAG, "onCreate changepass");
        mAuth = FirebaseAuth.getInstance();
        Log.d(TAG, "mAuth changepass");

        initializeUI();

        Intent newInt = getIntent();
        userEmail = newInt.getStringExtra(EXTRA_EMAIL);

        change_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("restart", "click");

                changePassword();
            }
        });
    }

    private void changePassword() {
        Log.d(TAG, "changepass start");

        final String current_pass_text, new_pass_text, confirm_pass_text;
        current_pass_text = current_pass.getText().toString();
        new_pass_text = new_pass.getText().toString();
        confirm_pass_text = confirm_pass.getText().toString();

        if (TextUtils.isEmpty(current_pass_text)) {
            Toast.makeText(getApplicationContext(), "Please enter current password", Toast.LENGTH_LONG).show();
        }
        if (TextUtils.isEmpty(new_pass_text)) {
            Toast.makeText(getApplicationContext(), "Please enter new password!", Toast.LENGTH_LONG).show();
        }
        if (TextUtils.isEmpty(confirm_pass_text)) {
            Toast.makeText(getApplicationContext(), "Please enter confirm password!", Toast.LENGTH_LONG).show();
        }

        if (new_pass_text.equals(confirm_pass_text)) {
            final FirebaseUser user = mAuth.getCurrentUser();

            if (user != null && userEmail != null) {
                // Get auth credentials from the user for re-authentication. The example below shows
                // email and password credentials but there are multiple possible providers,
                // such as GoogleAuthProvider or FacebookAuthProvider.
                AuthCredential credential = EmailAuthProvider
                        .getCredential(userEmail, current_pass_text);

                // Prompt the user to re-provide their sign-in credentials
                user.reauthenticate(credential)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    user.updatePassword(new_pass_text).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d(TAG, "Password updated");
                                            } else {
                                                Log.d(TAG, "Error password not updated");
                                            }
                                        }
                                    });
                                } else {
                                    Log.d(TAG, "Error auth failed");
                                }
                            }
                        });
            } else {
                startActivity(new Intent(this, BanoBike_Login.class));
            }
        } else {
            Toast.makeText(this, "Password didnt match, please try again", Toast.LENGTH_LONG).show();
        }
    }

    private void initializeUI() {
        current_pass = findViewById(R.id.et_current_pass);
        new_pass = findViewById(R.id.et_new_pass);
        confirm_pass = findViewById(R.id.et_confirm_pass);
        change_pass = findViewById(R.id.btn_change_pass);
    }
}
