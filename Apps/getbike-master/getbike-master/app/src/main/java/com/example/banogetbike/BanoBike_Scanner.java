package com.example.banogetbike;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AlertDialog;
import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class BanoBike_Scanner extends AppCompatActivity {
    private ImageView ivBgContent;
    private CodeScanner mCodeScanner;
    private CodeScannerView scannerView;
    private RequestQueue requestQueue;
    public static String EXTRA_ADDRESS = "device_address";
    public static String EXTRA_ID = "device_id";

    private String TAG = "ScannerActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bano_bike__scanner);

        requestQueue = Volley.newRequestQueue(this);

        int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int currentMinute = Calendar.getInstance().get(Calendar.MINUTE);

        if ((currentHour == 17) && (currentMinute > 30)) {
            showAlertDialog("GetBike tutup :(");
        } else if ((currentHour < 6) || (currentHour > 17)) {
            showAlertDialog("GetBike tutup :(");
        }

        initializeUI();

        ivBgContent.bringToFront();

        Log.d("restart", "got here tho");
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("restart", "got here tho resultresult");
                        Log.d("restart", result.getText());
                        // request to database
                        String key = "Bn2020Xyz";
                        String url = "http://178.128.120.241/api/getdev?key=" + key + "&qrcode=" + result.getText();
                        Log.d(TAG, url);

                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            String status = response.getString("status");

                                            if (status.equals("success")) {
                                                Log.d("restart", "status success");

                                                // get device object from response
                                                JSONObject device = response.getJSONObject("device");
                                                Log.d(TAG, String.valueOf(device));

                                                String address = device.getString("UID_BLE");
                                                String id = device.getString("id_devices");
                                                int batt = Integer.parseInt(device.getString("battery"));
                                                Log.d("restart", "id = " + id);

                                                if (batt > 20) {
                                                    // Make an intent to start next activity.
                                                    Intent i = new Intent(BanoBike_Scanner.this, BanoBike_Control.class);

                                                    //Change the activity.
                                                    i.putExtra(EXTRA_ADDRESS, address); //this will be received at ledControl (class) Activity
                                                    i.putExtra(EXTRA_ID, id);
                                                    startActivity(i);
                                                } else {
                                                    // device battery too low
                                                    showAlertDialog("battery too low, please try other bike");
                                                }
                                            } else {
                                                // failed to request data
                                                String ket = response.getString("ket");
                                                Log.e(TAG, ket);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            Log.e(TAG, e.toString());
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                Log.e(TAG, error.toString());
                            }
                        });

                        requestQueue.add(request);
                    }
                });
            }
        });

        checkCameraPermission();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkCameraPermission();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }

    private void checkCameraPermission() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mCodeScanner.startPreview();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                                   PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .check();
    }

    private void showAlertDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setCancelable(true);

        builder.setNegativeButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void initializeUI() {
        ivBgContent = findViewById(R.id.ivBgContent);
        scannerView = findViewById(R.id.scannerView);
    }
}
