package com.example.banogetbike;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class BanoBike_Profile extends AppCompatActivity {
    private TextView uname, emailTV;
    private EditText phone;
    private ImageView numberedit;
    private Button logout, change_password;

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;

    private DatabaseReference databaseReference;

    private String TAG = "ProfileActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bano_bike__profile);

        Log.d("restart", "onCreate Profile");
        mAuth = FirebaseAuth.getInstance();
        Log.d("restart", "mAuth Profile");

        initializeUI();
    }}

    @Override
    protected void onStart() {
        super.onStart();

        databaseReference = FirebaseDatabase.getInstance().getReference("Users");
        currentUser = mAuth.getCurrentUser();

        // Set value listener
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d("restart", "onDataChange Profile");
                User user = dataSnapshot.child(currentUser.getUid()).getValue(User.class);

                uname.setText(Objects.requireNonNull(user).getUsername());
                emailTV.setText(user.getUseremail());
                phone.setText(user.getUsermobileno());
                Log.d("restart","onDataChange done profile");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("restart", databaseError.toString());
                Log.d(TAG, databaseError.toString());
            }
        });

        // logout option
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(BanoBike_Profile.this, BanoBike_Login.class));
            }
        });

        // change password option
        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BanoBike_Profile.this, BanoBike_ChangePassword.class);
                intent.putExtra(BanoBike_ChangePassword.EXTRA_EMAIL, emailTV.getText());
                startActivity(intent);
            }
        });

        // update phone number
        final String newNumberText = phone.getText().toString();
        numberedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty((newNumberText))){
                    String userid, username, useremail;
                    userid = currentUser.getUid();
                    username = currentUser.getDisplayName();
                    useremail = currentUser.getEmail();

                    updateDatabase(new User(userid, username, useremail, newNumberText));

                    Toast.makeText(getApplicationContext(), "Phone number is updated", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Failed to update phone number", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void updateDatabase(User user){
        databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(currentUser.getUid());

        databaseReference.setValue(user);
    }

    private void initializeUI(){
        uname = findViewById(R.id.username);
        emailTV = findViewById(R.id.email);
        phone = findViewById(R.id.number);
        numberedit = findViewById(R.id.editnumber);
        logout = findViewById(R.id.logout);
        change_password = findViewById(R.id.btn_change_password);
    }
}
