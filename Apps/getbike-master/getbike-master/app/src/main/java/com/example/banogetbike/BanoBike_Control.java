package com.example.banogetbike;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;

import androidx.appcompat.app.AlertDialog;

import java.util.Objects;
import java.util.UUID;

public class BanoBike_Control extends Activity {
    //    private final String DEVICE_NAME="MyBTBee";
    private String DEVICE_ADDRESS;
    private String DEVICE_ID;
    private static final UUID BLUETOOTH_LE_CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private static final UUID BLUETOOTH_LE_NRF_SERVICE = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    private static final UUID BLUETOOTH_LE_NRF_CHAR_RW2 = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
    private static final UUID BLUETOOTH_LE_NRF_CHAR_RW3 = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");

    private BluetoothDevice device;
    private BluetoothGatt bluetoothGatt;
    private BluetoothGattCharacteristic readCharacteristic, writeCharacteristic;
    private ProgressDialog progressDialog;
    private String TAG = "ControlActivity";

    public static String EXTRA_COST = "total_cost";

    Button unlockButton, disconnectButton, paymentButton;
    boolean deviceConnected = false;
    boolean alreadyStart = false;
    boolean lock = false;
    byte[] buffer;

    // timer
    TextView timer;
    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L;
    Handler time, alertHandler;
    int Seconds, Minutes, MilliSeconds;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bano_bike__control);

        Intent newInt = getIntent();
        DEVICE_ADDRESS = newInt.getStringExtra(BanoBike_Scanner.EXTRA_ADDRESS); //receive the address of the bluetooth device
        DEVICE_ID = newInt.getStringExtra(BanoBike_Scanner.EXTRA_ID);
        time = new Handler();
        alertHandler = new Handler();

        unlockButton = findViewById(R.id.unlockbutton);
        disconnectButton = findViewById(R.id.disconnect);
        paymentButton = findViewById(R.id.paymentbutton);
        timer = findViewById(R.id.tvTimer);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please lock the device first");

        unlockButton.setText("Connecting");

        // initialize bluetooth
        BTinit();
        BTconnect();

        unlockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (deviceConnected) {
                    Log.d(TAG, "UNLK");
                    sendUnlock();
                } else {
                    Toast.makeText(getApplicationContext(), "Connecting... please wait", Toast.LENGTH_LONG).show();
                }
            }
        });

        paymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bluetoothGatt == null) {
                    return;
                }

                if (alreadyStart) {
                    progressDialog.show();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while (!lock) {
                            }

                            disconnect();

                            // go to payment
                            Intent intent = new Intent(BanoBike_Control.this, BanoBike_Payment.class);
                            intent.putExtra(EXTRA_COST, calculateCost());
                            startActivity(intent);
                            finish();

                            progressDialog.cancel();
                        }
                    }).start();
                }
            }
        });

        disconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alreadyStart) {
                    Toast.makeText(getApplicationContext(), "Tidak bisa disconnect saat timer berjalan, tolong dilock terlebih dahulu", Toast.LENGTH_LONG).show();
                } else {
                    disconnect();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        // Not allow to go back without disconnect
        Toast.makeText(this, "Sepeda harus didisconnect terlebih dahulu", Toast.LENGTH_LONG).show();
    }

    public void BTinit() {
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = null;

        // Determine whether BLE is supported on the device. Then
        // you can selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "Device doesn't support BLE", Toast.LENGTH_SHORT).show();
            finish();
        }
        if (bluetoothManager != null) {
            bluetoothAdapter = bluetoothManager.getAdapter();
            device = bluetoothAdapter.getRemoteDevice(DEVICE_ADDRESS);
        }
        if (bluetoothAdapter != null && !bluetoothAdapter.isEnabled()) {
            Intent enableAdapter = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableAdapter, 0);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void BTconnect() {
        bluetoothGatt = device.connectGatt(this, false, gattCallback);
        bluetoothGatt.connect();
    }

    private void sendUnlock() {
        if (!deviceConnected) {
            Toast.makeText(this, "not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            byte[] data = (DEVICE_ID + ";UNLK").getBytes();
            if (bluetoothGatt != null) {
                writeCharacteristic.setValue(data);
                bluetoothGatt.writeCharacteristic(writeCharacteristic);
            } else {
                Log.e(TAG, "not connected");
            }
        } catch (Exception e) {
            Log.e(TAG, Objects.requireNonNull(e.getMessage()));
        }
    }

    private void eksekusi() {
        String respon = new String(buffer);
        switch (respon) {
            case "true":
                if (!alreadyStart) {
                    alreadyStart = true;
                    start();
                    showDialogSukses();
                }
                break;
            case "low":
                showDialogGagal();
                break;
            case "false":
                showDialogIdsalah();
                break;
            case "lock":
                if (alreadyStart) {
                    Log.d(TAG, "lock");
                    pause();
                    lock = true;
                    showDialogLock();
                }
                break;
        }
        buffer = new byte[0];
    }

    private void showDialogSukses() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("Berhasil Unlock");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Selamat menikmati perjalanan dan berhati-hati")
                .setIcon(R.drawable.ic_spekun_hitam)
                .setCancelable(false)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    private void showDialogGagal() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("Low Battery");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Silakan Scan sepeda lain")
                .setIcon(R.drawable.ic_spekun_hitam)
                .setCancelable(false)
                .setNeutralButton("Scan", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        disconnect();
                        finish();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    private void showDialogLock() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("Berhasil lock");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Terimakasih telah bersepeda bersama kami, total biaya: " + calculateCost())
                .setIcon(R.drawable.ic_spekun_hitam)
                .setCancelable(false)
                .setNeutralButton("Go to Payment", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        disconnect();
                        Intent intent = new Intent(BanoBike_Control.this, BanoBike_Payment.class);
                        intent.putExtra(EXTRA_COST, calculateCost());
                        startActivity(intent);
                        finish();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    private void showDialogIdsalah() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("Koneksi Error");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Silakan scan ulang atau scan sepeda lain")
                .setIcon(R.drawable.ic_spekun_hitam)
                .setCancelable(false)
                .setNeutralButton("Scan", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        disconnect();
                        finish();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    private int calculateCost() {
        int cost = 3500;

        Seconds = (int) (UpdateTime / 1000);
        Minutes = Seconds / 60;

        if (Minutes > 30) {
            Minutes -= 30;
            cost += (Minutes / 5) * 1000;
        }

        return cost;
    }

    private void disconnect() {
        device = null;
        readCharacteristic = null;
        writeCharacteristic = null;
        if (bluetoothGatt != null) {
            bluetoothGatt.disconnect();
            try {
                bluetoothGatt.close();
            } catch (Exception ignored) {
            }

            bluetoothGatt = null;
            deviceConnected = false;
        }
        finish(); //return to the first layout
    }


    private void start() {
        StartTime = SystemClock.uptimeMillis();
        time.postDelayed(runnable, 0);
    }

    private void pause() {
        TimeBuff += MillisecondTime;
        time.removeCallbacks(runnable);
    }

    private final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, "connect with gatt");

                //Discover services
                gatt.discoverServices();

                deviceConnected = true;
                unlockButton.setText("UNLOCK");
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.e(TAG, "disconnected");
                disconnect();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                BluetoothGattService gattService = bluetoothGatt.getService(BLUETOOTH_LE_NRF_SERVICE);

                BluetoothGattCharacteristic rw2 = gattService.getCharacteristic(BLUETOOTH_LE_NRF_CHAR_RW2);
                BluetoothGattCharacteristic rw3 = gattService.getCharacteristic(BLUETOOTH_LE_NRF_CHAR_RW3);
                if (rw2 != null && rw3 != null) {
                    int rw2prop = rw2.getProperties();
                    int rw3prop = rw3.getProperties();
                    boolean rw2write = (rw2prop & BluetoothGattCharacteristic.PROPERTY_WRITE) != 0;
                    boolean rw3write = (rw3prop & BluetoothGattCharacteristic.PROPERTY_WRITE) != 0;
                    if (rw2write && rw3write) {
                        Log.e(TAG, "multiple write characteristics (" + rw2prop + "/" + rw3prop + ")");
                    } else if (rw2write) {
                        writeCharacteristic = rw2;
                        readCharacteristic = rw3;
                    } else if (rw3write) {
                        writeCharacteristic = rw3;
                        readCharacteristic = rw2;
                    } else {
                        Log.e(TAG, "no write characteristic (" + rw2prop + "/" + rw3prop + ")");
                    }
                }

                gatt.setCharacteristicNotification(readCharacteristic, true);
                BluetoothGattDescriptor desc = readCharacteristic.getDescriptor(BLUETOOTH_LE_CCCD);
                try {
                    desc.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    gatt.writeDescriptor(desc);
                } catch (Exception e) {
                    Log.e(TAG, Objects.requireNonNull(e.getMessage()));
                }
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            if (characteristic == readCharacteristic) {
                buffer = readCharacteristic.getValue();
                Log.d(TAG, new String(buffer));
                alertHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        eksekusi();
                    }
                });
            }
        }
    };

    public Runnable runnable = new Runnable() {

        @SuppressLint({"SetTextI18n", "DefaultLocale"})
        public void run() {
            MillisecondTime = SystemClock.uptimeMillis() - StartTime;
            UpdateTime = TimeBuff + MillisecondTime;

            Seconds = (int) (UpdateTime / 1000);
            Minutes = Seconds / 60;
            Seconds = Seconds % 60;
            MilliSeconds = (int) (UpdateTime % 1000);

            timer.setText("" + Minutes + ":"
                    + String.format("%02d", Seconds) + ":"
                    + String.format("%03d", MilliSeconds));

            time.postDelayed(this, 0);
        }

    };
}
