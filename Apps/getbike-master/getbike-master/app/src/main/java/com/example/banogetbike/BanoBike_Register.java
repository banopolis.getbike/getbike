package com.example.banogetbike;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.FileObserver;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class BanoBike_Register extends AppCompatActivity {

    private EditText emailTV, passwordTV,numberP,uname;
    private Button regBtn;
    private ProgressBar progressBar;

    private FirebaseAuth mAuth;

    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bano_bike__register);

        mAuth = FirebaseAuth.getInstance();

        initializeUI();

        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerNewUser();
            }
        });
    }

    private void registerNewUser() {
        progressBar.setVisibility(View.VISIBLE);

        String email, password, notelp, name;

        email = emailTV.getText().toString();
        password = passwordTV.getText().toString();
        notelp = numberP.getText().toString();
        name = uname.getText().toString();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Please enter Email...", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Please enter Password!", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(notelp)) {
            Toast.makeText(getApplicationContext(), "Please enter Phone Number!", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(getApplicationContext(), "Please enter Username!", Toast.LENGTH_LONG).show();
            return;
        }

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Registration successful!", Toast.LENGTH_LONG).show();
                    adddatabase();
                    progressBar.setVisibility(View.GONE);

                    Intent intent = new Intent(BanoBike_Register.this, BanoBike_Login.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Registration failed! Please try again later", Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    private void adddatabase(){
        //database

        String nama = uname.getText().toString().trim();
        String mail = emailTV.getText().toString().trim();
        String mobilenumber = numberP.getText().toString().trim();

        if (!TextUtils.isEmpty(nama)) {
            if (!TextUtils.isEmpty(mail)) {
                if (!TextUtils.isEmpty(mobilenumber)) {

                    //it will create a unique id and we will use it as the Primary Key for our User
                    String id = databaseReference.push().getKey();

                    FirebaseUser currentuser = mAuth.getCurrentUser();
                    String uid = Objects.requireNonNull(currentuser).getUid();
                    //creating an User Object
                    User User = new User(id, nama, mail, mobilenumber);
                    //Saving the User
                    databaseReference.child(uid).setValue(User);

                    //uname.setText("");
                    //numberP.setText("");
                    //emailTV.setText("");
                    Toast.makeText(this, "User added", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Please enter a mobilenumber", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(this, "Please enter a Email", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Please enter a name", Toast.LENGTH_LONG).show();
        }
    }

    private void initializeUI() {
        emailTV = findViewById(R.id.email);
        passwordTV = findViewById(R.id.password);
        regBtn = findViewById(R.id.register);
        progressBar = findViewById(R.id.progressBar);
        uname = findViewById(R.id.username);
        numberP = findViewById(R.id.number);

        databaseReference = FirebaseDatabase.getInstance().getReference("Users");
    }
}