-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 27, 2020 at 08:56 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `banopolis`
--

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id_devices` int(11) NOT NULL,
  `nama_devices` varchar(100) NOT NULL,
  `UID_BLE` varchar(30) NOT NULL,
  `qrcode` varchar(100) DEFAULT NULL,
  `lat_sepeda` decimal(11,8) DEFAULT NULL,
  `lon_sepeda` decimal(11,8) DEFAULT NULL,
  `speed` int(5) NOT NULL,
  `lock_sepeda` varchar(20) DEFAULT NULL,
  `battery` int(5) NOT NULL,
  `datetime_sepeda` varchar(50) NOT NULL,
  `waktu_update` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id_devices`, `nama_devices`, `UID_BLE`, `qrcode`, `lat_sepeda`, `lon_sepeda`, `speed`, `lock_sepeda`, `battery`, `datetime_sepeda`, `waktu_update`) VALUES
(1, 'Sepeda 1', '0A:33:2F:C4:FF:12', 'adfvzxgwrgv', '-6.75223456', '107.51123456', 0, 'false', 88, 'null', 1577344339),
(15, 'sepeda 2', '09:33:12:FB:AF:3C', '1wfad23r', '-6.65223456', '107.41123456', 0, 'true', 77, 'null', 1577344529),
(16, 'sepeda 3', '33:43:12:FA:AF:3E', 'asdfgrweq', '-6.12345600', '107.12345600', 0, 'true', 80, '2020-01-07/05:28:13', 1578380629);

-- --------------------------------------------------------

--
-- Table structure for table `histori`
--

CREATE TABLE `histori` (
  `id_histori` int(11) NOT NULL,
  `id_devices` int(11) NOT NULL,
  `lat_sepeda` decimal(11,8) NOT NULL,
  `lon_sepeda` decimal(11,8) NOT NULL,
  `status` varchar(30) NOT NULL,
  `battery` int(5) NOT NULL,
  `waktu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `histori`
--

INSERT INTO `histori` (`id_histori`, `id_devices`, `lat_sepeda`, `lon_sepeda`, `status`, `battery`, `waktu`) VALUES
(8, 1, '0.00000000', '0.00000000', '-', 0, 1573640105),
(9, 1, '0.00000000', '0.00000000', '-', 0, 1573640268),
(10, 2, '0.00000000', '0.00000000', '-', 0, 1573640298),
(11, 2, '0.00000000', '0.00000000', '-', 0, 1573640960),
(12, 16, '-6.12345600', '107.12345600', 'LOCK', 80, 1577344257),
(13, 16, '-6.75223456', '107.51123456', 'UNLOCK', 88, 1577344325),
(14, 1, '-6.75223456', '107.51123456', 'UNLOCK', 88, 1577344339),
(15, 11, '-6.75223456', '107.51123456', 'UNLOCK', 88, 1577344351),
(16, 11, '-6.75223456', '107.51123456', 'UNLOCK', 88, 1577344381),
(17, 11, '-6.75223456', '107.51123456', 'UNLOCK', 88, 1577344421),
(18, 15, '-6.65223456', '107.41123456', 'LOCK', 77, 1577344527),
(19, 15, '-6.65223456', '107.41123456', 'LOCK', 77, 1577344529),
(20, 16, '-6.12345600', '107.12345600', 'LOCK', 80, 1578380629);

-- --------------------------------------------------------

--
-- Table structure for table `secret_key`
--

CREATE TABLE `secret_key` (
  `id_key` int(11) NOT NULL,
  `key` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `secret_key`
--

INSERT INTO `secret_key` (`id_key`, `key`) VALUES
(1, 'Bn2020Xyz');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `avatar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `username`, `password`, `avatar`) VALUES
(1, 'Tyto Mulyono', 'tyto@tytomulyono.com', 'tyto', '$2a$08$3WyRJUHBqEG.sQ4yYTLxqOAXyqApz5/4AMZ73kauVsah1QfyKe7yC', 'logo.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id_devices`);

--
-- Indexes for table `histori`
--
ALTER TABLE `histori`
  ADD PRIMARY KEY (`id_histori`);

--
-- Indexes for table `secret_key`
--
ALTER TABLE `secret_key`
  ADD PRIMARY KEY (`id_key`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id_devices` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `histori`
--
ALTER TABLE `histori`
  MODIFY `id_histori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `secret_key`
--
ALTER TABLE `secret_key`
  MODIFY `id_key` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
