<?php
$this->load->View('include/header.php');

if ($set=="setting") {
  $skey = "";

?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Setting
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>admin/setting"><i class="fa fa-gear"></i> Setting</a></li>
        <!-- <li class="active">Lihat Histori Device</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <?php echo $this->session->flashdata('pesan');?>
            </div>
            <!-- /.box-header -->


            <div class="box-body table-responsive">
              <div class="callout callout-success">
                <h4><i class="icon fa fa-warning"></i> SECRET KEY</h4>

                <?php
                foreach ($key as $keys => $value) {
                  $skey = $value->key;
                  echo "<i class='icon fa fa-lock'></i> <b>".$skey."</b>";
                }
                ?>
              </div>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-link"></i> URL UPDATE DATA DEVICE</h4>

                <i class='icon fa fa-globe'></i> <b><?=base_url();?>api/updatedev?key=<?=$skey;?>&iddev=XXX&status=LOCK&batt=80&lat=-6.123456&lon=107.123456</b>
              </div>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php
} 

$this->load->view('include/footer.php');
?>

</div>  <!-- penutup header -->

<!-- jQuery 3 -->
<script src="<?=base_url();?>components/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url();?>components/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url();?>components/dist/js/adminlte.min.js"></script>

<!-- DataTables -->
<script src="<?=base_url();?>components/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>components/plugins/datatables/dataTables.bootstrap.min.js"></script>

</body>
</html>