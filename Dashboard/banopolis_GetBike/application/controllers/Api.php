<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('m_admin');
        $this->load->model('m_api');
        date_default_timezone_set("asia/jakarta");
    }

	public function index()
	{
		echo "REST API for Device";
		// $varx = "http://banopolis.test/api/updatedev?key=Bn2020Xyz&iddev=XX&status=LOCK&batt=80&lat=-6.123456&lon=107.123456&speed=0&dtime=2020-01-07/05:28:13";
		// echo "<br>";
		// echo strlen($varx);
	}

	//http://banopolis.test/api/updatedev?key=Bn2020Xyz&iddev=XX&status=LOCK&batt=80&lat=-6.123456&lon=107.123456&speed=0&dtime=2020-01-07/05:28:13
	public function updatedev(){
		if (isset($_GET['key']) && isset($_GET['iddev']) && isset($_GET['lat']) && isset($_GET['lon']) && isset($_GET['batt']) && isset($_GET['status']) 
			&& isset($_GET['speed']) && isset($_GET['dtime'])) {
			$key = $this->input->get('key');
			$cekkey = $this->m_api->getkey();
			//print_r($cekkey);
			if($cekkey[0]->key == $key){
				$iddev = $this->input->get('iddev');
				$lat = $this->input->get('lat');
				$lon = $this->input->get('lon');
				$batt = $this->input->get('batt');
				$status = $this->input->get('status');
				$speed = $this->input->get('speed');
				$dtime = $this->input->get('dtime');

				if($status == "LOCK"){
					$lock = "true";
				}else{
					$lock = "false";
				}
				
				$data = $this->m_api->getdevice($iddev);

				$x = 0;
				if (isset($data)) {
					foreach ($data as $key => $value) {
						if($value->id_devices == $iddev){
							$x++;
						}
					}
					if($x>0){
						$array = array('lock_sepeda' => $lock, 'lat_sepeda' => $lat, 'lon_sepeda' => $lon, 
							'battery' => $batt, 'waktu_update' => time(), 'speed' => $speed, 'datetime_sepeda' => $dtime, );
						$arrayhistori = array('id_devices' => $iddev, 'status' => $status, 'lat_sepeda' => $lat, 'lon_sepeda' => $lon, 'battery' => $batt, 'waktu' => time());
						if ($this->m_api->updatedevice($iddev,$array)) {
							$this->m_api->insert_histori($arrayhistori);
							$array = array('status' => 'success', 'ket' => 'berhasil update data');
							echo json_encode($array);
						}else{
							$array = array('status' => 'error', 'ket' => 'gagal update data');
							echo json_encode($array);
						}
					}else{
						$array = array('status' => 'error', 'ket' => 'id device tidak ditemukan');
						echo json_encode($array);
					}
				}else{
					$array = array('status' => 'error', 'ket' => 'id device tidak ditemukan');
					echo json_encode($array);
				}
			}else{
				$array = array('status' => 'error', 'ket' => 'salah secret key');
				echo json_encode($array);
			}
		}else{
			$array = array('status' => 'error', 'ket' => 'salah parameter');
			echo json_encode($array);
		}
	}


}
