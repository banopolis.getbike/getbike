<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('./application/third_party/phpoffice/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Admin extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('m_admin');
        $this->load->library('bcrypt');
        date_default_timezone_set("asia/jakarta");
    }

	
	public function index()
	{
		redirect(base_url().'admin/dashboard');
	}

	public function dashboard(){
		$data['set'] = "dashboard";

		$this->load->view('v_dashboard', $data);
	}

	public function list_users(){
		$data['set'] = "list-users";
		$data['data'] = $this->m_admin->get_users();
		$this->load->view('v_users', $data);
	}

	public function add_users(){
		$data['set'] = "add-users";
		$this->load->view('v_users', $data);
	}


	public function save_users(){
		if($this->session->userdata('userlogin')){
			$users = $this->input->post('users');
			$email = $this->input->post('email');
			$username = $this->input->post('username');
			$pass = $this->input->post('pass');
			$hash = $this->bcrypt->hash_password($pass);

	        $type = explode('.', $_FILES["image"]["name"]);
			$type = strtolower($type[count($type)-1]);
			$imgname = uniqid(rand()).'.'.$type;
			$url = "components/dist/img/".$imgname;
			if(in_array($type, array("jpg", "jpeg", "gif", "png"))){
				if(is_uploaded_file($_FILES["image"]["tmp_name"])){
					if(move_uploaded_file($_FILES["image"]["tmp_name"],$url)){
						$data = array(
				                'nama'    => $users,
				                'email'   => $email,
				                'username'=> $username,
				                'password'=> $hash,
				                'avatar'  => $imgname,
				        );
						$this->m_admin->insert_users($data);
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di simpan</div>");
					}
				}
			}else{
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di simpan, ekstensi gambar salah</div>");
			}
	        
			redirect(base_url().'admin/list_users');
		}
	}

	
	public function hapus_users($id=null){
		if($this->session->userdata('userlogin'))     // mencegah akses langsung tanpa login
		{ 
			$path = "";
			$filename = $this->m_admin->get_user_byid($id);
			foreach ($filename as $key) {
				$file = $key->avatar;
				$path = "components/dist/img/".$file;
			}
			
			//echo $path;

			if(file_exists($path)){
				unlink($path);
				if($this->m_admin->users_del($id)){
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di hapus</div>");
				}else{
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di hapus</div>");
				}
			}else{
				if($this->m_admin->users_del($id)){
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di hapus image gagal dihapus</div>");
				}else{
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di hapus</div>");
				}
			}

			redirect(base_url().'admin/list_users');
		}
	}


	public function edit_users($id=null){
		if($this->session->userdata('userlogin')){     // mencegah akses langsung tanpa login
			if (isset($id)) {
				$user = $this->m_admin->get_user_byid($id);
				foreach ($user as $key => $value) {
					//print_r($value);
					$data['id'] = $id;
					$data['nama'] = $value->nama;
					$data['email'] = $value->email;
					$data['username'] = $value->username;
					$data['password'] = $value->password;
					$data['avatar'] = $value->avatar;
				}
				$data['set'] = "edit-users";
				$this->load->view('v_users', $data);

			}else{
				redirect(base_url().'admin/list_users');
			}
		}
	}

	public function save_edit_users(){
		if($this->session->userdata('userlogin')){     // mencegah akses langsung tanpa login
			if (isset($_POST['id']) && isset($_POST['email'])) {
				$id = $this->input->post('id');
				$email = $this->input->post('email');
				$nama = $this->input->post('users');
				$username = $this->input->post('username');
				$pass = $this->input->post('pass');
				$hash = $this->bcrypt->hash_password($pass);


				$type = explode('.', $_FILES["image"]["name"]);
				$type = strtolower($type[count($type)-1]);
				$imgname = uniqid(rand()).'.'.$type;
				$url = "components/dist/img/".$imgname;
				if(in_array($type, array("jpg", "jpeg", "gif", "png"))){
					if(is_uploaded_file($_FILES["image"]["tmp_name"])){
						if(move_uploaded_file($_FILES["image"]["tmp_name"],$url)){
							$data = array(
					                'nama'    => $users,
					                'email'   => $email,
					                'username'=> $username,
					                'avatar'  => $imgname,
					        );
					        $file = $this->input->post('img');
							$path = "components/dist/img/".$file;

							if(file_exists($path)){
								unlink($path);
							}
							$this->m_admin->updateUser($id, $data);
							$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di simpan</div>");
						}
					}
				}else{
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di simpan, ekstensi gambar salah</div>");
				}

				if(isset($_POST['changepass'])){
					$data = array(		'email' => $email,
										'nama' => $nama,
										'username'=> $username,
						                'password'=> $hash,
				 				);
					if ($this->m_admin->updateUser($id,$data)) {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
					}else{
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di update</div>");
					}
				}else{
					$data = array(		'email' => $email,
										'nama' => $nama,
										'username'=> $username,
				 				);
					if ($this->m_admin->updateUser($id,$data)) {
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
					}else{
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di update</div>");
					}				
				}

				redirect(base_url().'admin/list_users');
			}
		}
	}


	public function devices(){
		$data['set'] = "devices";
		$data['devices'] = $this->m_admin->get_devices();

		$this->load->view('v_devices', $data);
	}

	public function add_devices(){
		$data['set'] = "add-devices";
		$this->load->view('v_devices', $data);
	}

	public function save_devices(){
		if($this->session->userdata('userlogin')){
			$id = $this->input->post('id');
			$nama = $this->input->post('nama');
			$ble = $this->input->post('ble');
			$lat = $this->input->post('lat');
			$lon = $this->input->post('lon');

			//$duplicate = $this->m_admin->get_devices_byid_row($id);
			//$hasil = count($duplicate);


			if (false) {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> ID Alat sudah terdaftar, ganti ID Alat</div>");
			}else{
				$data = array(
		                'nama_devices'  => $nama, 'UID_BLE'  => $ble, 'lat_sepeda' => $lat, 'lon_sepeda' => $lon, 'waktu_update' => 0, 'status' => "AVAILABLE",
		        );
							
				if($this->m_admin->insert_devices($data)){
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di simpan</div>");

				}else{
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di simpan</div>");
				}
			}
	        
			redirect(base_url().'admin/devices');
		}
	}

	public function hapus_devices($id=null){
		if($this->session->userdata('userlogin'))     // mencegah akses langsung tanpa login
		{ 
			if($this->m_admin->devices_del($id)){
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di hapus</div>");
			}else{
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di hapus</div>");
			}
			
			redirect(base_url().'admin/devices');
		}
	}

	public function edit_devices($id=null){
		if($this->session->userdata('userlogin')){     // mencegah akses langsung tanpa login
			if (isset($id)) {
				
				$devices = $this->m_admin->get_devices_byid($id);
				if (isset($devices)) {
					foreach ($devices as $key => $value) {
						//print_r($value);
						$data['id'] = $value->id_devices;
						$data['nama_devices'] = $value->nama_devices;
						$data['ble'] = $value->UID_BLE;
						$data['qrcode'] = $value->qrcode;
					}
					$data['set'] = "edit-devices";
					$this->load->view('v_devices', $data);
				}
				
			}else{
				redirect(base_url().'admin/devices');
			}
		}
	}


	public function save_edit_devices(){
		if($this->session->userdata('userlogin')){     // mencegah akses langsung tanpa login
			if (isset($_POST['id']) && isset($_POST['nama']) && isset($_POST['qrcode'])) {
				$id = $this->input->post('id');
				$nama = $this->input->post('nama');
				$ble = $this->input->post('ble');
				$qrcode = $this->input->post('qrcode');

				$data = array('nama_devices' => $nama, 'UID_BLE' => $ble, 'qrcode' => $qrcode,
			 				);

				if ($this->m_admin->updateDevices($id,$data)) {
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data berhasil di update</div>");
				}else{
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Data gagal di update</div>");
				}
				redirect(base_url().'admin/devices');
			}
		}
	}


	public function histori(){
		$data['set'] = "histori";
		$data['histori'] = $this->m_admin->get_history();

		$this->load->view('v_histori', $data);
	}


	public function hapus_histori(){
		if($this->session->userdata('userlogin'))     // mencegah akses langsung tanpa login
		{ 

			if($this->m_admin->empty_data()){
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Histori berhasil di hapus</div>");
			}else{
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\" id=\"alert\"><i class=\"glyphicon glyphicon-ok\"></i> Histori gagal di hapus</div>");
			}

			redirect(base_url().'admin/histori');
		}
	}


	
	public function setting()
	{
		$data['set'] = "setting";
		$data['key'] = $this->m_admin->getkey();
		//print_r($data);
		$this->load->view('v_setting', $data);
	
	}

	
}